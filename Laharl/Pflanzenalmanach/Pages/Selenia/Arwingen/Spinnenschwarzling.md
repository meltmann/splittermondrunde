#  Spinnen-Schwarzling

Beschreibung des Pilzes

## Fundorte

Hier, da, dort

## Seltenheit

Und so

## Nutzung

### Alchemie

Wird zur Herstellung eines lokalen Giftes `Schwarzer Arwinger` genutzt.

### Heilwirkung

Wirkt schmerzunterdrückend.
(Schmerzwiderstand II für 5 Minuten oder 400 Ticks)

Einnahme kann statt dessen zu einer Vergiftung führen.
(Einnahme 2/Benommen/5 Minuten oder 400 Ticks)
