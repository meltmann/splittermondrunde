# Rituale, Segnungen, Gebete und der ganze Rest

## Teeritual

#### Utensilien

- Muschelblaues seidenes Teetuch
- Porzellanteetässchen **ohne** Henkel
- Beliebiger bodennaher Tisch
- Kurzfloriger Teppich
- Stößel und Mörser
- Kotopfeffer
- Feuerblatt

#### Vorbereitung

- Der Kotopfeffer wird mittels Stößel im Mörser klein gemahlen
- Der Tisch wird rituell immer wieder mit den behaarten Unterarmen von der längsseitigen Mitte zum Rand hin gereinigt.
- Die Tasse wird mit dem zu verwendenden Feuerblatt kreisförmig im Sonnenuhrzeigersinn gereinigt
- Während beider Reinigungsvorgänge ist jeweils die traurige Geschichte **Ohito**s auf Xienjan zu singen, der bis in alle Ewigkeit verdammt ist, dieses Teeritual erfolglos zu wiederholen 
- Die Tasse wird im unteren Drittel des Tisches längsseits mittig platziert
- Dann werden zunächst der gemahlene Kotopfeffer und danach das Feuerblatt in der Tasse platziert, wobei das Feuerblatt glatt aufliegt und die Spitze nach Osten zeigt
- Anschließend wird das seidene Teetuch eine Hand breit links neben die Teetasse gelegt, so dass eine Spitze des Teetuchs ebenfalls nach Osten zeigt.

#### Durchführung

- Es ist der Lotussitz einzunehmen
- Das kochende Wasser ist im Lotussitz in die Tasse einzugießen
- Kein heißes Wasser darf außerhalb der Teetasse den Tisch berühren. Auch nicht in einer Kanne!
- Der Tee muss in exakt 20 Minuten getrunken werden!

#### Dauer

Die Dauer beträgt exakt 40 Minuten, da das Wasser ohne Zutun während der anderen Vorbereitungen vor dem Trinken kochen kann:

- 5 Minuten zieht der Tee
- 5 Minuten werden die Zutaten für das kochende Wasser vorbereitet
- 5 Minuten wird die Teetasse rituell gereinigt
- 5 Minuten wird der Tisch rituell gereinigt
- 20 Minuten trinkt er andächtig

### Wirkung bei Erfolg

- Zauber `Ausdauer stärken`
- Gilt als Teezeremonie für die Meisterschaft `Fundament der Seele`

## Gebete

Shangguans Gebete führen regeltechnisch einen Zauber aus.
Der ganze regeltechnische Proben-, Vorbereitungs-, Auslöse- und Verstärkungskrams gilt entsprechend üblicher Zauber, für Patzer wird die oberste Patzertabelle für Priester auf Seite 200 des Grundregelwerks zu Rate gezogen.

| Spruch                                                     | Zauber                      |
| ---------------------------------------------------------- | --------------------------- |
| Gib mir die Gelassenheit interessante Zeiten zu überstehen | `Ausdauer stärken`          |
| Leihe mir die Unbeirrbarkeit Deiner geheiligten Kruste     | `Aura der Entschlossenheit` |
| Sichere mein Überleben bei dieser Auseinandersetzung       | `Lebenskraft stärken`       |
| Lass dieses Duell fair verlaufen                           | `Geisterdolch`              |
| Leihe mir die Härte Deiner geheiligten Kruste              | `Dornenfaust`               |
| Verschaffe mir einen Vorteil in diesem Duell               | `Geisterwaffe`              |

## Segnungen

Da aktuell die Zauber lediglich auf den Zauberer selbst wirken, sind Segnungen ohne praktischen Nutzen.
Im Prinzip sehen sie aus wie die Gebete, nur halt ohne eine Wirkung. Weiterhin wird Shangguan nur Segnungen der Stärkung aussprechen.

- Mögest Du auch in interessanten Zeiten Deine Gelassenheit behalten
- Möge Dich die Unbeirrbarkeit Juaseis geheiligter Kruste leiten 
- Mögest Du diese Auseinandersetzung überleben

Gruppensegnungen sind nicht vorgesehen. 
Etwas derart Unanständiges wurde im Kloster zur Krabbe nicht gelehrt.

# Wertetabellen

## Detaillierte Waffenwerte

| Waffe                       | Geschwindigkeit | Schaden | Merkmale                                                     | Details                       |
| --------------------------- | --------------- | ------- | ------------------------------------------------------------ | ----------------------------- |
| Krallen (waffenlos)         | 5               | 1W6     | Entwaffnend 1, Umklammern (Stumpf entfällt wgn. Schwäche)    | GRW Charakterbogen Archetypen |
| Geisterdolch                | 6               | 1W6+1   | Wurffähig                                                    | GRW 183                       |
| Geisterwaffe "Drachenfaust" | 12              | 2W10+2  | Ablenkend 1, Lange Waffe, Scharf 3, Umklammern, Unhandlich, Wuchtig, Zweihändig | Mondstahlklingen 15           |



## Meisterschaften

### Kampf

| Name                          | Schwelle | Kampffertigkeit | Wirkung                                                      | Details                                      |
| ----------------------------- | -------- | --------------- | ------------------------------------------------------------ | -------------------------------------------- |
| Block                         | 1        | Handgemenge     | Der Abenteurer kann die Fertigkeit *Handgemenge* nutzen, um eine *aktive Abwehr* gegen einen bewaffneten Gegner durchzuführen. | GRW 96                                       |
| Harter Stil (Kampfstil)       | 1        | Handgemenge     | Die unbeugsamen Angriffe des Abenteurers bringen seine Feinde immer wieder ins Taumeln. Verursacht er bei einem Angriff nur einen *Streiftreffer*, verliert der Gegner zusätzlich zu den üblichen Auswirkungen einen Tick. Dieser Kampfstil muss nicht vorher angekündigt werden. | Zhoujiang 121; *Streiftreffer* siehe GRW 161 |
| Verwirrung (Manöver)          | 1        | Handgemenge     | Dieser *Nahkampfangriff* geht gegen den *Geistigen Widerstand* des Gegners und richtet bei Erfolg nur haben Schaden an. Dafür erhält der nächste *Angriff* gegen denselben Gegner (egal von wem) einen Bonus in Höhe von 2 Punkten. Der Angreifer kann weitere Erfolgsgrade einsetzen, um den Bonus um 1 je Erfolgsgrad zu erhöhen. Ein Kämpfer kann gleichzeitig nur Ziel einer *Verwirrung* sein. | GRW 96                                       |
| Verteidigungswirbel (Manöver) | 1        | Kettenwaffen    | Zusätzlich zu seinem regulären *Nahkampfangriff* bringt der Kämpfer sich mit diesem *Manöver* in eine vorteilhafte Defensivposition. Für den nächsten *Angriff* gegen seine *Verteidigung* in diesem Kampf ist besagte *Verteidigung* um 2 Punkte erhöht. Zudem kann er weitere Erfolgsgrade nutzen, um seine *Verteidigung* je eingesetztem Erfolgsgrad um einen weiteren Punkt zu erhöhen. | GRW 97                                       |



### Allgemein

| Name                  | Schwelle | Allgemeine Fertigkeit | Wirkung                                                      | Details    |
| --------------------- | -------- | --------------------- | ------------------------------------------------------------ | ---------- |
| Ausweichen I          | 1        | Akrobatik             | Die *Verteidigung* des Abenteurers erhöht sich um 2 Punkte, wenn er weder Rüstung noch Schild noch Parierwaffe trägt, er sich des Angriffs bewusst ist und ihm ausreichend Platz zur Verfügung steht. | GRW 101    |
| Fundament der Seele   | 1        | Entschlossenheit      | Durch eine halbstündige meditative Tätigkeit (Meditation, stilles Gebet, ritueller Tanz, Teezeremonie oder dergleichen) stärt der Abenteurer seinen Geist und versichert sich der Nähe seines Gottes. Er legt eine *Entschlossenheitsprobe* gegen 25 ab. Bei Erfolg kann er in der nächsten Stunde eine Stufe des Zustands *Glaubenskrise* ignorieren, auch wenn dieser erst nach der Meditation erlangt wird. Jeder Erfolgsgrad erhöht die Dauer um 1 Stunde. | Götter 122 |
| Langstreckenschwimmer | 1        | Schwimmen             | Der Schwimmer erreicht beim Langstreckenschwimmen (GRW 129) pro Erfolgsgrad eine Strecke von 4 km statt 2 km | GRW 129    |
| Wachsamkeit           | 1        | Zähigkeit             | Der Abenteurer kann ohne Probleme eine Nacht wach bleiben und zum Beispiel Wache schieben. Erst bei einer weiteren Nachtohne Schlaf muss er eine *Zähigkeit*-Probe ablegen. | GRW 137    |



## Waffenmerkmale

| Waffenmerkmal       | Bedeutung                                                    | Details             |
| ------------------- | ------------------------------------------------------------ | ------------------- |
| Ablenkend [Stufe]   | Die Waffe ist biegsam und daher schwer vorhersehbar. Die Grundschwierigkeit einer *Aktiven Abwehr* gegen diese Waffe erhöht sich um 5 pro Stufe des Merkmals. | Mondstahlklingen 47 |
| Exakt [Stufe] | Bei einem Schadenswurf mit dieser Waffe werden so viele Würfel zusätzlich geworfen, wie die Stufe des Merkmals beträgt. Diese zusätzlichen Würfel sind die gleiche Würfelart, die beim Schadenswurf der Waffe verwendet wird. Die höchsten Ergebnisse zählen für den Schadenswurf, wobei nur so viele Würfel gewertet werden, wie beim Schaden der Waffe angegeben sind. | Mondstahlklingen 47  |
| Entwaffnend [Stufe] | Für jede Stufe dieses Merkmals zählt die Last dieser Waffe um 1 Punkt erhöht, wenn es um die erlaubte Nutzung des *Manövers Entwaffnung* geht. | GRW 186             |
| Lange Waffe         | Die Waffe ermöglicht es, aus einer größeren Distanz anzugreifen. Ein gegnerisches *Lösen aus dem Kampf* erhält einen Malus in Höhe von 3 Punkten, ein eigenes einen Bonus in Höhe von 3 Punkten. | GRW 186             |
| Scharf [Stufe]      | Alle Schadenswürfel einer Waffe mit diesem Merkmal werden immer als **mindestens** der Wert des Stufe des *Merkmals* gewertet, egal was eigentlich gewürfelt wurde. Dies gilt **nicht** für zusätzliche Schadenswürfel aus *Manövern*, *Zaubern* oder sonstigen Quellen. | GRW 186             |
| Stumpf              | Der von der Waffe verursachte Schaden zählt vollständig als *Betäubungsschaden*. Dies gilt auch für zusätzlichen Schaden aus *Manövern*, nicht aber aus Quellen wie etwa *Zaubern* | GRW 186             |
| Umklammern          | Mit dieser Waffe ist das Manöver *Umklammern* einsetzbar.    | GRW 186             |
| Unhandlich          | Die *Verteidigung* des Trägers erhält bei Führung dieser Waffe einen Malus in Höhe von 2 Punkten. | GRW 186             |
| Wuchtig             | Bei Waffen mit diesem Merkmal verursacht das *freie Manöver Wuchtangriff* 2 statt 1 zusätzlichen Punkt Schaden pro eingesetztem Erfolgsgrad. | GRW 186             |
| Wurffähig           | Die *Nahkampfwaffe* kann auch als *Wurfwaffe* mit eigenen Werten verwendet werden. | GRW 186             |
| Zweihändig          | Die Waffe muss mit zwei Händen geführt werden.               | GRW 186             |

## Stärken

| Stärke                     | Bedeutung                                                    | Details    |
| -------------------------- | ------------------------------------------------------------ | ---------- |
| Asket                      | Der Abenteurer pflegt einen asketischen Lebensstil, fastet häufig und ist es gewöhnt, weltlichem Komfort zu entsagen. Zur Bestimmung der Zeitspanne, bevor er zu verhungern beziehungsweise verdursten beginnt (GRW 175), ist seine Konstitution um einen Punkt erhöht. Versuche, ihn mit weltlichen Genüssen zu verlocken, erhalten einen *leicht negativen Umstand*. Darüber hinaus verbraucht er bei Überlandreisen (GRW 151) weniger Proviant. Er kann dadurch die Kosten zur Verpflegung für sich selbst um die Hälfte reduzieren und kommt bei der Nahrungssuche mit einer halben Ration aus. | Götter 111 |
| Ausdauernd                 | Der Abenteurer kommt bei anstrengenden Tätigkeiten nicht so leicht aus der Puste, sei es im Kampf oder auf einem langen Marsch. Bei *Zähigkeit*s-Proben zur Abwehr des Zustands *Erschöpft* erhält er einen Bonus von 3 Punkten. Außerdem werden Abzüge durch den Zustand *Erschöpft* halbiert. Für die Berechnung von *Überanstrengung* (GRW 175) gilt die **Konstitution** als verdoppelt. | GRW 72     |
| Erhöhter Fokuspool         | Der Abenteurer vermag es, ein größeres Maß an magischer Kraft durch seinen Körper zu leiten. Er erhält 5 zusätzliche Fokuspunkte. Diese Stärke ist beliebig oft wählbar. | GRW 72     |
| Flink                      | Der Abenteurer verfügt über eine angeborene Schnelligkeit. Seine **Geschwindigkeit** steigt um 1 Punkt. | GRW 72     |
| Priester                   | Der Abenteurer ist ein geweihter Priester seiner Gottheit. Er bezieht Magie nicht mehr aus der Umgebung sondern direkt und ausschließlich von seinem Gott. Dadurch hat seine Magie nie negativen Einfluss auf die Umwelt gemäß der Angaben der Tabelle zu *Fokus und Umgebung* (GRW 197). Im Gegenzug kann sein Gott ihn bei groben Verstößen gegen die Prinzipien des Kultes den Zugang zur Magie temporär verwehren – die Entscheidung dazu liegt letzten Endes beim Spielleiter. Bei Patzern würfelt er außerdem nicht auf die Patzertabelle für Magier, sondern für Priester (GRW 200). Er erhält niemals echten Schaden durch fehlgeschlagene Zauber, das Band zu seinem Gott und sein Selbstvertrauen könnten aber erschüttert werden. | GRW 73     |
| Natürlicher Rüstungsschutz | Die Haut des Abenteurers ist von einem dichten Fell bedeckt, was ihm eine **Schadensreduktion** von 1 einbringt. | GRW 73     |
| Natürliche Waffen          | Der Abenteurer verfügt über scharfe Krallen oder spitze Zähne, die er im Kampf einsetzen kann. Im unbewaffneten Kampf kann er nach Wahl echten Schaden anstelle von Betäubungsschaden anrichten. | GRW 73     |

## Schwächen

| Name           | Wirkung                                                      | Details     |
| -------------- | ------------------------------------------------------------ | ----------- |
| Fellerneuerung | Das Fell des Varg muss nicht geschnitten werden, da es sich in regelmäßigen Abständen erneuert. Heimlichkeitsproben auf *Verstecken* oder *Spuren verwischen* sind um 6 erschwert, wenn sie den Abenteurer decken können sollen. | Spieleridee |
| Feste Krallen  | Eine genetische Abweichung raubt dem Varg die Fähigkeit, seine Krallen einzuziehen. Heimlichkeitsproben auf *Schleichen* sind um 6 erschwert, wenn sich der Abenteurer auf festem Untergrund befindet. (Holzplanken, Marmor, Stein, Felsen…) Bei weichen natürlichen Untergründen wie Moos, Gras oder Erde greift dieser Malus *nicht*. Weiterhin entfällt die initiale Wahl des *Betäubungsschaden*s im unbewaffneten Kampf, so dass der Abenteurer für *Betäubungsschaden* einen Erfolgsgrad aufbringen muss. | Spieleridee |
| Leichtgläubig  | Menschenkenntnis gehört nicht zu den Stärken des Abenteurers, vielmer ist er durch Naivität udn Leichtgläubigkeit geprägt. Er wird von gewieften Händlern über den Tisch gezogen, zweifelt nicht an leeren Versprechungen und glaubt selbst die dreisteste Lüge, die ihm aufgetischt wird. | GRW 84      |
| Vorurteile     | Der Abenteurer verfügt über ein festgefahrenes und äußerst schlechtes Bild einer bestimmten Gruppierung, beispielsweise ein Volk oder eine Spezies. Wird er mit ihren Vertretern konfrontiert, ist er rationalen Argumenten gegenüber nicht mehr zugänglich und macht sie für alles verantwortlich, das auch nur ansatzweise negativ ist. | GRW 84      |
| Weichherzig    | Pragmatisches Denken ist dem Abenteurer fremd, wenn es um den Umgang mit seinen Feinden geht. Aus purem Gutmenschentum heraus lässt er besiegte Straßenräuber lieber ziehen, die ihm gerade noch einen Hinterhalt gelegt haben, und vertuscht Beweise, um die Bestrafung eines Bösewichts zu verhindern. | GRW 84      |



## Ungeheuer

Die Aufstellung folgt dem Vorbild aus dem Kapitel **Erläuterung der Wertangaben** im Grundregelwerk, S. 263ff. und ist um die Angabe der *Quelle* erweitert.

### Drachenschildkröte

TBC

## Zauber

Die Aufstellung ist vom Vorbild aus dem Kapitel **Erläuterung der Zauberbeschreibung** im Grundregelwerk, S. 222 inspiriert und um die Angabe der *Quelle* erweitert.

### Dornenfaust (Spruch)

#### Typus

Hand, Pflanzen

#### Kosten: 

Normal K4V1, verstärkt K5V2

#### Wirkung

Die Hand des Zauberers wird borkig und mit Dornen gespickt. Im unbewaffneten Nahkampf richten seine Angriffe echten Naturschaden an und besitzen das *Waffenmerkmal Exakt 1*. Dieser Zauber ist nicht mit anderen Zaubern des Typs *Hand* kombinierbar.

#### Verstärkung

Für 2 Erfolgsgrade und zusätzlich K1V1 richtet der Zauberer im unbewaffneten Nahkampf einen zusätzlichen Punkt echten Naturschaden an und seine Faust erhält das Waffenmerkmal *Scharf 3*

#### Quelle

Zhoujiang, Seite 123

## Zustände

### Erschöpft [Stufe], GRW 168

Der Abenteurer ist ausgelaugt. Alle seine Proben auf *Kampffertigkeiten* und *Allgemeine Fertigkeiten* erhalten eine Erschwernis von 1 Punkt pro Stufe. Die **Geschwindigkeit** ist um 1 Punkt pro Stufe gesenkt, die **Initiative** um 1 Punkt pro Stufe erhöht. Dieser Effekt ist stapelbar. Der Zustand wird im Normalfall durch eine *Verschnaufpause* abgebaut.

### Glaubenskrise [Stufe], GRW 168

*Dieser Zustand kann durch Patzer für Priester erreicht werden. Vergleiche Patzertabelle GRW 200!*

Das Vertrauen des Priesters in die Verbindung zu einer Gottheit ist temporär gestört, wodurch er sich schlechter auf seine Magie konzentrieren kann. Alle Proben auf Magieschulen erhalten einen Malus in Höhe der Stufe. Dieser Effekt ist stapelbar. Der Zustand wird im Normalfall nach einer *Verschaufpause* aufgehoben.

