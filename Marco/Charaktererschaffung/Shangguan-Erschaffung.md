# Charaktererschaffung

## Schritt 1: Idee

Ein Mönch irgend einer Gottheit, der den waffenlosen Kampf bevorzugt.

Dabei ist er tendenziell in sich ruhend, fast schon stoisch, beherrscht seinen Körper, lebt asketisch und beherrscht sowohl göttliche (magische…) Fähigkeiten des Kampfes, der Stärkung und ein bisschen Schicksal.

-----

## Schritt 2: Varg

| Wert                       |          Art |
| :------------------------- | -----------: |
| +2 Stärke                  |     Attribut |
| +1 beliebiges *Anderes*    |     Attribut |
| -1 Willenskraft            |     Attribut |
| Ausdauernd                 |       Stärke |
| Natürliche Waffen          |       Stärke |
| Natürlicher Rüstungsschutz |       Stärke |
| Sehr groß (6)              | Größenklasse |


| Aussehen   | Beschreibung                                                 |  Wurf |
| ---------- | ------------------------------------------------------------ | ----: |
| Fellfarbe  | Schwarz                                                      |  9, 0 |
| Augenfarbe | Bernsteinfarben mit grauen Sprenkeln, eigenmächtig auf Eisblau geändert |  7, 9 |
| Größe      | 2 Meter 22 Zentimeter, also 2,22m                            | 5+8+9 |
| Gewicht    | 125 Kilogramm                                                |     – |



Quelle: Grundregelwerk

## Schritt 3: Zhoujiang

| Wert                     |           Art |
| :----------------------- | ------------: |
| Xienjan                  | Muttersprache |
| Zhoujiang                |   Kulturkunde |
| Soziales Gespür          |        Stärke |
| Arkane Kunde 2           |    Fertigkeit |
| Diplomatie 1             |    Fertigkeit |
| Empathie 2               |    Fertigkeit |
| Entschlossenheit 1       |    Fertigkeit |
| Geschichten und Mythen 2 |    Fertigkeit |
| Länderkunde 1            |    Fertigkeit |
| Redegewandtheit 2        |    Fertigkeit |
| Überleben 2              |    Fertigkeit |
| Wahrnehmung 1            |    Fertigkeit |
| 1 Magieschule            |    Fertigkeit |
| Feilscher (Diplomatie)   | Meisterschaft |

Quelle: Zhoujiang



-----

## Schritt 4: Seefahrer

| Fertigkeit  | Punkte |
| :---------- | -----: |
| Handgemenge |      1 |
| Länderkunde |      1 |
| Schwimmen   |      1 |
| Seefahrt    |      1 |
| Wahrnehmung |      1 |

| Ressource | Punkte |
| :-------- | -----: |
| Kreatur   |      1 |
| Mentor    |      1 |

Quelle: Grundregelwerk





-----

## Schritt 5: Kampfkünstler

| Wert                            |                Art |
| :------------------------------ | -----------------: |
| Flink                           |             Stärke |
| Gleichgewichtssinn              |             Stärke |
| Relikt 1                        |          Ressource |
| Mentor 1                        |          Ressource |
| Handgemenge 3                   |         Fertigkeit |
| 2 weitere Kampffertigkeiten 3   |         Fertigkeit |
| Akrobatik 3                     |         Fertigkeit |
| Geschichten und Mythen 1        |         Fertigkeit |
| Entschlossenheit 3              |         Fertigkeit |
| Zähigkeit 2                     |         Fertigkeit |
| Bewegungsmagie 4                |         Fertigkeit |
| Stärkungsmagie 3                |         Fertigkeit |
| 1 weitere Magieschule 2         |         Fertigkeit |
| Block (Handgemenge)             | Kampfmeisterschaft |
| Harter Stil *oder* Weicher Stil | Kampfmeisterschaft |

Quelle: Zhoujiang

-----

## Schritt 6: Attribute

18  Punkte, jedes minimum 1, maximum 3

| AUS  | BEW  | INT  | KON  | MYS  | STÄ  | VER  | WIL  | Gesamt |
| ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ------ |
| 1    | 3    | 1    | 3    | 3    | 3    | 1    | 3    | 18/18  |



-----

## Schritt 7: Stärken, Ressourcen, freie Fertigkeitspunkte

| Stärke             | Kosten |
| ------------------ | ------ |
| Erhöhter Fokuspool | 2/3    |
| Priester           | 0/3    |
| Asket              | 1/3    |

| Ressource | Kosten |
| --------- | ------ |
| Relikt    | 1/2    |
| Kreatur   | 1/2    |

| Freie Fertigkeitspunkte | Menge |
| ----------------------- | ----- |
| Akrobatik               | 3/5   |
| Handgemenge             | 2/5   |

Quellen: Grundregelwerk, Die Götter

-----

## Schritt 8: Mondsplitter und Schwächen

### Mondzeichen

Der **Fels**:

- Wenn der höchste Wurf des Sicherheitswurfes eine 5 zeigt, darf der Spieler doch beide Würfel addieren
- **Verstärkt**: Für 2 Splitterpunkte darf ein dritter W10 geworfen und dazuaddiert werden, wenn der höchste Würfel 5 zeigt
- **Geheime Gabe**: Ein Gefährte darf nachträglich einen Sicherheitswurf als Standardwurf werten

### Schwächen

- Feste Krallen
- Fellerneuerung (a.k.a. *haart*)
- Leichtgläubig
- Weichherzig
- Vorurteile

-----

## Schritt 9: Starterfahrung

| Wert        | Punkte | Kosten |
| ----------- | ------ | ------ |
| Handgemenge | 2      | 6/15   |
| Zähigkeit   | 3      | 9/15   |

-----

## Schritt 10: Feinschliff

-----

