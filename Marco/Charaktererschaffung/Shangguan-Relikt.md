# Beseelter Gegenstand

Ein muschelblauer Geisterseidengürtel ist um Shangguans Taille geschlungen, um seine Hose zu halten.

## Gottheit

*Juasei* in seiner Rolle als Kreatur des Meeres

## Heiligenseele

*Jui Wu Xia* war die wichtigste diplomatische Vertreterin Juaseis während der *Urda*-Dynastie.
Sie bereiste die Kontinente, rettete unzählige Schiffbrüchige, richtete noch unzähligere Seebestattungen aus und soll der Legende nach im Alleingang eine Drachenschildkröte erlegt haben, die ihr Schiff attackierte.

Sie vermied unnötige Kämpfe, doch stellte sich jeder Herausforderung.
Dabei achtete sie stets auf die Einhaltung der wichtigsten Regel auf See: Gehorsam.

## Gebote

- *Respekt*: An jedem fließenden Gewässer beten
- *Reinheit*: Rituelle Waschung an jedem Morgen durchführen

## Verbote

- *Gehorsam*: Niemals gegen einen Kapitän aufbegehren

-----

# Kräfte

## Günstige Winde (I) — ausstehend

### Queste

- jeweils 6 Fertigkeitspunkte in **Schwimmen** und **Seefahrt** erreichen

### Wirkung

- Seereisen mit dem Träger dieses Artefakts an Board sind 20% schneller.

##  Kälte der Tiefsee (II) — ausstehend

### Queste

- Einem Sturm von mehreren Tagen auf einem Schiff trotzen

### Wirkung

- Einmal pro Spielsitzung kann der Träger zwei Stunden lang die Kältestufe in bis zu 50 Metern Umkreis um sich um 1 Stue senken. Er selbst ist auf Wunsch von den Effekten ausgenommen.



## Herr der Wale (III+) — ausstehend

### Queste

- Eine Drachenschildkröte im Kampf besiegen

### Wirkung

- Der Träger kann einmal pro Abenteuer eine große Meereskreatur als Transportmittel für sich und seine Gruppe rufen.

-----

Quelle: Die Götter, Seite 137