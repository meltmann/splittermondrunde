# Aussehen und Wirkung auf Fremde

> Wie sieht der Abenteurer aus – und wie wirkt er auf Fremde?

## Der Körper an sich

Im Vergleich zum landläufig bekannten Varg fallen seine gedrungene Kopf- und Schnauzenform auf, die anscheinend in keinem Verhältnis zu den riesigen Ohren stehen.

Die Fellzeichnung dieses mit über 2,20 Metern verhältnismäßig großem Exemplar ist kaum erkennbar, da sein Deckfell tiefschwarz und seine hellere Vorderseite noch immer dunkel anthrazitfarben ist. Ebenso sind die sichtbaren Flächen seiner Haut, also die Nase und die erhabenen Pfotenballen, von dunklem Anthrazit.

Im Gegensatz zur düsteren Farbgebung stechen die nahezu kristallseeklaren eisblauen Augen aus dem Dunkel des Gesichts hervor.

Shangguans Statur lässt sich am Ehesten mit den Adjektiven "drahtig" und "sehning" beschreiben.

Vor Allem bei starkem Wind fällt auf, dass er zu den wenigen Vargen mit sich regelmäßig erneuerndem Fell zählt.

Loraker mit sehr guten Augen können bei guter Sonneneinstrahlung oder speziellen blauen Lampions Zhoujiangs weiße gleißend-leuchtende feine Linien wahrnehmen, die sich von den Fingerzwischenräumen beginnend an Shangguans Unterarmen bis zu den Ellenbogen hochziehen und dort in einem zusammenhängenden Gespinst mit jeweils drei kleinen Raupen enden.

Interessierten erzählt er gern, wie er auf der Seidenstraße auf dem Weg zum Mondportal und nach Sarnburg von einer gnomischen Edelhandwerkerin ob seiner dunklen Haut angesprochen wurde. Sie wollte an unsichtbaren Stellen ein paar Linien mit einer neuen Wunderfarbe ausprobieren, die auch Zhoujiangis mit dunkler Hautfarbe in den Genuss der Tätowierkunst kommen lassen sollte, die auf Grund der eher dunklen bunten Farbtöne bis dato nur den Hellhäutigen vorbehalten war.

Zhoujiangis indes stellen ihm diese Frage nie und kennen daher diese Antwort auch nicht, die in der Fremde selbst den introvertiertesten Vertreter zu einem erstaunten Heben einer Augenbraue animieren würden.

## Körpersprache

Shangguan zeichnet sich durch eine hohe Körperspannung aus.

Er scheint einerseits jede als unnötig erachtete Bewegungen gänzlich zu vermeiden, andererseits begiebt er sich plötzlich in die seltsamsten Haltungen und Positionen. 

Wer der Kampfeskunst nicht gänzlich abgeneigt ist, erkennt sofort die Kata aus dem Chi Kung.

Zhoujiangische Beobachter werden diese irgendwo zwischen Fangschrecke, Krebs und Salamander einordnen.

## Die Kleidung

Gekleidet ist er in eine dunkelviolette Tuchhose mit sehr weitem Beinstoff, die ihm bis zur Mitte der Schienbeine reicht.
Lediglich bei merklicher Kälte bedeckt er seinen Oberkörper mit einem ebenso gefärbten Oberteil aus Tuch, das allerdings die Arme inklusive Schultern frei lässt und über der Brust einen V-Ausschnitt bis zum Schlüsselbein zeigt. 

## Waffen und Rüstungen

Er bestaunt und begutachtet zwar regelmäßig und neugierig die geschmiedeten Waffen der Welt und probiert sie durchaus auch einmal, um sie mit den ihm bekannten Werkstücken aus Zhoujiang zu vergleichen, doch in Kämpfen scheint er diese geschmiedete Kunst abzulehnen.

Dabei geht er waffenlosen Kämpfen konsequent aus dem Weg. Er zeigt sich nur zu einem Duell bereit, wenn sein Gegenüber mit einer Waffe ausgestattet ist. Fragt man ihn nach dem Grund, gibt er an seine Krallen nicht einfahren zu können. Kratzende Geräusche auf unnatürlichem bearbeiteten Boden lassen diese Aussage durchaus glaubhaft erscheinen. Er meidet die typischen hölzernen Trippen aus Zhoujiang mit dem Hinweis auf die Notwendigkeit der Nähe seines Körpers zum Land seines Gottes und Ahnen.

Ob das hölzerne Getrippel jetzt angenehmer für die Hörenden wäre als das Krallengekratze sei einmal dahin gestellt.



# Zhoujiang und Die Provinz des Krebses

> In welchem Umfeld ist der Abenteurer aufgewachsen?

Geboren 968 LZ in Seimeidaa in der Provinz des Krebses (Ebigani) zum Ende der Drei-Reiskörner-Hungersnot und dem Beginn des Bürgerkrieges lässt sich über sein Aufwachsen noch nicht viel sagen.

Ebigani ist ein weißer Fleck, der vom Spielleiter noch ausgearbeitet wird.

Eckpunkte:

- Einziger Sohn zweier Vargen (beide Priester der Kranichgötting *Zuru*)
- Im Alter von 6 Jahren verwaist, da nach den Legenden die Dschunke mit seinen Eltern auf dem Jadesee von einer Drachenschildkröte attakiert und versenkt wurde
- In einem Kloster irgendwo noch nicht näher benannt zum "Mönch des Fangschreckenkrebses" ausgebildet
- Mit 19 Jahren das Kloster verlassen um als Wanderpriester Lorakis zu erkunden

-----

Wie ist Ihr Abenteurer aufgewachsen?
War seine Familie arm oder reich, lebte sie auf dem Land oder in der Stadt, auf dem Festland oder auf dem Meer?
Hatte er überhaupt eine Familie, oder ist er als einsames Waisenkind aufgewachsen?
Hatte er einen Mentor oder Vorbilder, zu denen er aufgeschaut hat?
Pflegt er noch Kontakt zu seiner Familie oder Freunden von früher, oder hat er sämtliche Verbindungen zu seiner Vergangenheit abgebrochen?
Wie sah sein Umfeld aus – und welchen Einfluss hatte es auf sein Benehmen von heute (Tischmanieren bis hin zu persönlicher Ehrvorstellung)?

-----

# Soziale Beziehungen

## Familie, Geliebte

> Hat der Abenteurer eine eigene Familie und/oder eine große Liebe?

Seine Eltern verstarben auf dem Jadesee durch den Angriff einer Drachenschildkröte, als Shangguan gerade einmal sechs Jahre jung war.

Danach lebte er als "Mönch des Fangschreckenkrebs" im Kloster und hatte dort kaum Kontakt zu anderen Mönchsschülern, die eine enge persönliche Bindung zugelassen hätten.

Zusätzlich sind die "Mönche der Fangschreckenkrebse" nicht unbedingt für ihre sozialen Fähigkeiten oder tiefer Empathie bekannt, so dass sich ihre Kontakte eher auf Oberflächlichkeit belaufen.

## Freunde, Feinde und andere Katastrophen

> Hat der Abenteurer einen besten Freund und/oder ärgsten Feind?

Auf seinen Reisen durch Zhoujiang lernte Shangguan eine Wugei zugewandte Exorzistin namens Quan Xi (Mensch, *956LZ, lockiges schwarzes Haar, lächelt viel, sorgfältig) kennen und schätzen.

Sie zeigte ihm die Wichtigkeit akribischer Vorbereitung in Bezug auf den Kontakt mit Geisterwesen und lehrte ihn den richtigen Gebrauch von Schutzgebeten und -amuletten. Allerdings stellte er sich bei den Gebeten eher tolpatschig an, so dass er lediglich den rituellen Umgang mit den Amuletten verinnerlichen konnte.

Sie reisten eine Zeit lang zusammen und er sieht sie als seine Mentorin und Unterstützerin bei Fragen rund um das Jenseits und Geister an.

Die jahrelange Ausbildung im Kloster zum "Mönch des Fangschreckenkrebs" hat ihn Gleichmut gelehrt, so dass er sich keiner Feinde bewusst ist.
Allerdings sind Zhoujiangis nie frei von Feinden, viele kennen nur noch nicht alle.

# Der Geist des Krebses

> Zu welchen Göttern betet der Abenteurer?

Shangguan hat sich und sein Handeln ganz dem Krebsgott *Juasei* verschrieben.

Wie alle tief religiösen Zhoujiangis sind im natürlich die Lehren der anderen 12 Tiergötter sowie ein paar der Entrückten rudimentär bekannt.

Durch die lange Zeit mit seiner Mentorin Quan Xi könnte er auch ohne zusätzliche Hilfestellung das jährliche hochfeierliche Ritual der Quell- und Brunnenreinigung abhalten. Da die Schildkrötengöttin Wugei allerdings die Durchführung durch eine:n ihrer Priester:innen erfordert, durfte er bisher ein mal einem jungen unerfahrenen Wugei Priester zur Hand gehen. Dass er hierbei auch die exakte Durchführung und die Segenssprüche vorsagte, ist Wugei entweder entgangen oder wurde als Grauzone billigend in Kauf genommen.

Sein Chi Kung kombiniert die impulsive Offensive der Fangschreckenkrebse mit der harten dornigen Schale des Hummers und der defensiven Schutzhaltung des Einsiedlerkrebses.

# Rituale, Gebete und Segnung

> Wie geht der Abenteurer mit Magie um?

Shangguan ist Magie höchst suspekt und er hält sie für gefährlich.
Allerdings ist Magie für ihn das, was landläufig als "Feenmagie" bezeichnet wird. Ein Wort, dass es in Zhoujiang auf Grund der Nähe zum Geister- und Ferne zum Feendiesseits nicht einmal existiert.

Die im gesamten Land betriebene Alltagsmagie nimmt er wohlwollend als Wirken der 13 Tiergötter zur Kenntnis.
Er ist teils belustigt, teils entsetzt, dass vor Allem die Barbaren des Westens die Gabe der Götter mit so wenig Dankbarkeit nutzen.

Naturzerstörung durch übermäßige Magie hat er noch nie gesehen, da scheinbar alle Zhoujiangis ihre magische Energie von den Tiergöttern verliehen bekommen.

Er selbst hält sich für magisch unbegabt und durch den Krebsgott beseelt.

Verbesserungen in seiner Konstitution, aber auch sichtbare Veränderungen an seinem Körper oder das Erscheinen halbdurchsichtiger geisterhafter Waffen in seiner Hand aus dem Nichts hält er für die göttliche Antwort auf seine Rituale und Gebete.

# Die Wanderschaft des Mönchs

> Wie kam der Abenteurer zum Abenteurerleben?

Während der Zeit im Kloster nervte Shangguan die ganze Politik und Bürokratie seines Reiches doch allmählich an.

Da er mitten in den Bürgerkrieg hineingeboren wurde und auch noch in jungen Jahren seine Eltern verlor, hält er von dem ganzen Geschwätz um Ahnen, Region und Einigkeit wenig. Zumal die im Landesinneren teilweise immer sichtbar werdenden Auseinandersetzungen der drei Parteien die Vermutung nahe legen, dass es alles nicht so einig ist im Land von Drachen und Phönix.

Mit 18 Jahren musste er sich langsam entscheiden, worauf er sich ab seinem 19. Geburtstag spezialisieren will:

- Eine attraktive und sichere Beamtenlaufbahn als Anwärter der "Priester des Taschenkrebses" mit der zeremoniellen Betreuung des Festes "Tag der Türme" und ansonsten dem unterzeichnen, stempeln und siegeln unglaublich langweiliger Dokumente
- Die Unterstützung der "Inquisitoren des Knallkrebses", die allerlei Meeresungetier von den Stränden Zhoujiangs in die Tiefen zurückdrängen
- Eine Flussschifffahrtskarriere als "Schützer des Flusskrebses", welche die Handelsschiffe auf den Flüssen vor Piraten und anderem Gesindel gegen gutes Gold verteidigen
- Die asketischen "Pilger der Wanderkrabbe", welche die Welt bereisen und im Namen *Juasei*s Gutes tun, um ihn in der Ordnung der Tiergeister noch ein bisschen höher an den Phönix heranrücken zu lassen.

Da ihm nur eine Opportunität die Gelegenheit bot, dieses seltsame Konstrukt aus Zusammenhalt und Zerrissenheit, Ordnung und Chaos sowie Tradition und Neuerung zu durchbrechen, verließ er mit seinem 19. Geburtstag das Kloster und reiste zunächst relativ ziellos durch Zhoujiang.

Am 14. Schildkrötenmond 988 traf er in Angshang auf eine Guwei-Priesterin, die die traditionelle Reinigung und Segnung der Brunnen und Quellen durchführte. Nach den Formalien der Feierlichkeit unterhielten sich die beiden abseits des Trubels und es entstand eine Art Band zwischen den Beiden.

Die nächsten 3 Jahre begleitete Shangguan Quan Xi auf ihren Reisen und lernte allerlei theoretisches über die Arbeit der "Stimmen der Geister" sowohl in der Kommunikation mit geliebten Ahnen sowie der erfolgreichen Vertreibung eher störender Exemplare dieser Gattung.

Dabei viel ihm jeder Exorzismus, der nicht zufällig gegen Poltergeister oder andere Spukerscheinungen ging, ausgesprochen schwer.
Quan Xi, die ihm in dieser Zeit Freundin und Mentorin wurde, machte oftmals Scherze über seine kommunikative Unfähigkeit. 
Es ist dabei anzunehmen, dass er weder die humorvollen noch die zynischen Bemerkungen diesbezüglich wirklich verstand.

991LZ beschließt Shangguan schließlich, Zhoujiang genug durchquert zu haben und macht sich auf den Weg nach Jagodien.
Reiseberichten zu Folge sollen die Humanoiden dort enge Bindungen zu ihren Pferden aufbauen und Shangguan befürchtet eine Stärkung der Pferdegöttin *Umasama die Zahme*, der Verrat an der Büffelgöttin *Sheiniu* beging.

# Das Gleichgewicht zwischen Drache und Phönix

> Was ist der sehnlichste Wunsch des Abenteurers?

Eigentlich wollte Shangguan seine Heimat nicht verlassen müssen. Die aktuelle innenpolitische Situation seines Landes zwang ihn dazu.

Vor Allem die Nähe zum Jadesee, die einzige sehr dünne Verbindung zu den Geistern seiner Eltern, liegt mittlerweile in weiter Ferne.

Er wünscht sich nichts sehnlicher, als dass Wu endlich an Yis Seite als gleichberechtigter General herrschen kann, ohne dass die beiden heiraten müssen.
Damit sei der Tradition des Phönix Zhongshi an der Seite der Prinzessin Genüge getan und das permanente Hin und Her des Gleichgewichts zwischen Drache und Phönix würde auf wenige Meter in einem großen Thronsaal beschränkt werden.

Es soll endlich die Stabilität und Gelassenheit in Zhoujiang einkehren, für die das Land seit Jahrtausenden kämpft.

# Deine Ängste werden Deine Stärke

> Wovor fürchtet sich der Abenteurer am meisten im Leben?

Die asketischen Lehren im Kloster des Krebses haben ihm die Zuversicht gegeben, auch in den interessantesten Zeiten sein inneres Gleichgewicht zu wahren. 

Lediglich Feenwesen sind ihm ausgesprochen suspekt.

Den Anführern der einzelnen Gruppen der Triaden möchte er auch lieber nicht einmal am helligten Tag in einem belebten Großstadtzentrum begegnen.

# Und die Moral von der Geschicht'…

> Wie sehen die Moralvorstellungen des Abenteurers aus?

Was moralisch richtig oder falsch ist liegt seiner Auffassung nach bei den Göttern und Ahnen.

Und hier beginnt das Dilemma. Die Spinnengöttin *Gagamba* findet Spionage völlig in Ordnung. Ihr zu Ehren findet am 1. Spinnenmond ein Festtag statt, in dem jeder Bürger Zhoujiangs versucht, irgend ein Geheimnis eines Nachbarn herauszufinden.

Die Katzengöttin *Pusa* ist vollkommen fein mit Betrug und anderen Hinterhältigkeiten, solange sie mindestens dem eigenen Vorteil gereichen.

Und während die Ahnen der einen Provinz das Teetrinken im Lotussitz als Todsünde abstrafen, kann es ein Haus weiter für beleidigte Geister sorgen, wagt ein Gast den Diamantsitz am Nachmittage einzunehmen.

Für Shangguan gibt es nur eine einzige Moral: Handelt ein Humanoid allen 13 Tiergeistern zuwider, gilt das Gottesurteil durch seine Pranken.

# Was ich nicht will, das man mir tut, das tu ich keinem Ander'n.

> Wie steht der Abenteurer zu Gewalt und dem Töten von Lebewesen?

Als "Pilger der Wanderkrabbe" sollte man annehmen, Shangguan beherrsche die wichtigsten sozialen Etiketten und könne sich in sozialen Konflikten rhetorisch, empathisch oder diplomatisch behaupten.

Dem ist allerdings nicht so. Eigentlich geht Shangguan sozialen Konflikten aus dem Weg.

Wenn das nicht klappt und er zwischen einigen Streitenden schlichten muss, holt er einen *Tsuchu* Ball aus seinem Seesack, stellt einen Stuhl in die Mitte eines von ihm je nach Untergrund mit Kreide oder Kohle markierten Bereichs und erklärt die Regeln dieses Ballspiels.
An dessen zeitlich begrenztem Ende steht dann der Sieger und damit Gewinner des Streits fest.

Sollte er zu einem Duell im Faustkampf gefordert werden, lehnt er freundlich, aber bestimmt, mit dem Hinweis auf seine starren Krallen ab.
Gibt sein Gegenüber nicht auf, lässt er durch ein Stoßgebet zu *Juasei* einen Geisterdolch in seiner Hand erscheinen, die er dem Gegenüber zur Verteidigung reicht. Meist schreckt das den Kontrahenten erfolgreich ab.

Manchmal allerdings klappt auch das nicht. Dann stellt er sich dem Kampf in einer defensiven Haltung, jederzeit Unachtsamkeiten des Gegners gnadenlos und gezielt ausnutzend, um ihn schnell mit möglichst wenig Blutvergießen zu Boden zu bringen.

# Die Bürde des Wandermönchs

> Pflegt der Abenteurer seltsame Verhaltensweisen oder Macken?

Seltsam an ihm scheint die strikte Abneigung von Schuhwerk und Oberkörperbekleidung.

Auch sein morgendliches Tee- und abendliches Pfeifenritual wirken befremdlich.

Ansonsten nutzt er jede freie Minute für mit starker Körperspannung einfach nur da sitzen oder seltsame Verrenkungen machend, die geschulten Augen als Chi Kung Katas auffallen.

# Von sinnvoller Aktivität und verschwendeter Ablenkung

> Versteht der Abenteurer Spaß?

Von Freizeitaktivitäten, die nicht der körperlichen Ertüchtigung dienen, hält Shangguan auch nichts.

Dabei ist es einerlei ob es das zhoujiangische Tsechu oder das selenische Holzwerfen ist, solange er Letzteres ohne Alkoholpflicht mitspielen darf.

Für Witze, Zynismus und Sarkasmus ist er eher wenig empfänglich. Sollte er einmal einen typischen Witz reißen, wird er von vielen nicht verstanden.

Beispiele:

*Genervtes Gegenüber*: "Dir ist aufgefallen, dass wir auf einem Schiff sind?" 
*Shangguan*: "Ach darum das viele Wasser um uns herum…"

# Traditionen über alles

> Ist der Abenteurer aufgeschlossen gegenüber Neuem?

Seine Aufgeschlossenheit beschränkt sich auf reine Neugier zum Vergleich mit Bekannten.
Traditionsbewusst fällt ihm entweder die gleichwertige Qualität wie in Zhoujiang auf, oder es ist neumodischer Krams, der halt nix taugt.

Das Miteinander von Humanoiden hingegen ist ihm vollkommen fremd, egal ob bekannt aus Zhoujiang oder seltsam aufgeschlossener gleichberechtigter Firlefanz aus Selenia.

# Zwei Rezepte für Glückseeligkeit: Verrate nicht alles.

> Was ist das tiefste Geheimnis des Abenteurers?

Shangguans Erzählungen bezüglich des Ursprungs seiner Tätowierungen entsprechen nicht ganz der Wahrheit.

Zunächst einmal war der tätowierende Gnom männlich. Dann fällt jedem Zhoujangi mit minimaler Straßenkunde sofort auf, dass es sich um ein typisches Gruppentattoo der Triade `Der Fließende Stein` unter der Leitung von My-Mei handelt.
Selbst der Hinweis, dass er am Straßenrand zur Erprobung der neuen Farbe angesprochen wurde, ist in zweierlei Hinsicht falsch.

Wahr ist vielmehr, dass *My-Mei* (Mensch, *961 LZ, kleinwüchsig, stlitiserte Tätowierungen von Seidenraupen und Spinnen, traditionelle Seidenrobe, lange Opiumpfeife) den Gnom *Ho Chu (*856 LZ, in sich ruhend, Kleidung von den Stromlandinseln) mit in das Kloster brachte. 

Die Mönche sollten sich als Zeichen der Wertschätzung der Unterstützung durch den `Fließenden Stein`das Triadentattoo stechen lassen. Ho Chu brannte darauf, die neuen Körperfarben an Geschöpfen mit dunkler Hautfarbe auszuprobieren und Shangguan bot sich dort an. Leider vergaß der Gnom um Erlaubnis zu fragen und Shangguan hat die Schmerzen, die er trotz Meditiation kaum ertragen konnte, bis heute nicht vergessen.

Das nächste bisher geheim gehaltene Erlebnis trat ein, als er My-Mei über den Weg lief.
Für den Bruchteil einer Sekunde schien die Welt um ihn herum stehen zu bleiben und unscharf zu werden: Nur My-Mei und er waren klar erkennbar.

Wann immer er versuchte den Dialog mit Anderen zu suchen, wurde er mit Strafgebeten und -meditationen belegt, so dass er auch dieses Geheimnis bis auf Weiteres für sich behält.

# Vom Geist des Krebses auserwählt

> Weiß der Abenteurer, dass er ein Splitter­träger ist, und wie beeinflusst ihn das?

Shangguan hat natürlich festgestellt, dass ihm einige der Kampfproben besser gelangen als er es ursprünglich angenommen hatte.

Für einen Auserwählten Juaseis empfindet er das jedoch als vollkommen normal. 